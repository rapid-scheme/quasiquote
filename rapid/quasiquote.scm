;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> \section{Introduction}

;;> This library defines an extension to \scheme{quasiquote} that allows
;;> ellipses to be used in place of \scheme{unquote-splicing}, which
;;> often leads to more readable code.

;;> \section{Examples}

;;> The basic usage of \scheme{quasiquote} is the same as in the R7RS:

;;> \example{`(list ,(+ 1 2) 4)}

;;> Lists can still be spliced into sequences.

;;> \example{`(a ,(+ 1 2) ,@(map abs '(4 -5 6)) b)}

;;> The extension to \scheme{quasiquote} allows ellipses to be used
;;> in place of \scheme{unquote-splicing} (\scheme{,@}) to piece together
;;> the output form.

;;> \example{`(a ,(+ 1 2) ,(map abs '(4 -5 6)) ... b)}

;;> Within each subform followed by an ellipsis, each comma-prefixed
;;> item must be a list and all such items within the same subform
;;> must have the same length.

;;> \example{`((,'(1 2 3) . ,'(a b c)) ...)}

;;> A subform followed by an ellipsis may be contained with a larger
;;> subform that is also followed by an ellipsis. In this case, each
;;> comma-prefixed item must be a list of lists, each such item must
;;> have the same length, and the corresponding sublists of each such
;;> item must have the same lengths. This requirement generalizes to
;;> comma-prefixed items nested within more than two levels of
;;> ellipsis-followed subforms.

;;> \example{`(((a ,'((x 1) (x 2) (x 3))) ...) ...)}

;;> In the output, a subform may be followed directly by two or more
;;> ellipses; the requirements are the same as for nested ellipses,
;;> but the result is flattened rather than nested.

;;> \example{`((a ,'((x 1) (x 2) (x 3))) ... ...)}

;;> Ellipses can also follow subforms containing items prefixed
;;> by comma-at.

;;> \example{`((a ,@'((x 1) (x 2) (x 3))) ...)}

;;> A subform of the form \scheme{(... form)} is identical to
;;> \scheme{form}, except that ellipses in the subform have no special
;;> meaning.

;;> \example{`(... (,'(1 2 3) ...))}

;;> Substitutions are made only for unquoted components appearing
;;> at the same nesting level as the outermost quasiquote.

;;> \example{`(a `(b ,(list 1 2) ... ,(foo ,(list 1 3) ... d) e) f)}

;;> \section{Syntax}

;;> \macro{(quasiquote form)\br{}
;;> `form}

;;> If no instances of the ellipsis \scheme{...} appear within the
;;> \var{form} at the same nesting level as the outmost quasiquote,
;;> the result of evaluating \scheme{`form} is
;;> the same as if \scheme{quasiquote} from
;;> \scheme{(scheme base)} was used.
;;>
;;> A subform of the form \scheme{(... form)} is identical to
;;> \scheme{form},
;;> except that ellipses within the subtemplate have no special meaning.
;;>
;;> If a subform is followed by one or more instances of the
;;> ellipsis, the expressions following a comma have to evaluate
;;> into nested lists of the same or higher nesting depths.
;;> They are replaced in the output by
;;> all of the elements they match in the input, distributed as
;;> indicated. It is an error if the output cannot be built up
;;> as specified.

(define-syntax quasiquote
  (syntax-rules ()
    ((quasiquote template)
     (quasiquote-aux template))
    ((quasiquote . args)
     (syntax-error "bad quasiquotation" (quasiquote args)))))

;;> \section{Auxiliary syntax}

;;> \macro{unquote\br{}
;;> ,
;;> unquote-splicing\br{}
;;> ,@
;;> ...}

;;> Syntax bindings equivalent to those in \scheme{(scheme base)}.

(define-syntax quasiquote-aux
  (syntax-rules ::: (quasiquote unquote unquote-splicing ...)
		((quasiquote-aux (... template))
		 (scheme-quasiquote template))
		((quasiquote-aux ,form)
		 form)
		((quasiquote-aux (form ... ... . rest))
		 (extract-vars (quasiquote-ellipses-repeated rest) (form ...)))
		((quasiquote-aux (,@form ... . rest))
		 (append (apply append form) `rest))
		((quasiquote-aux (form ... . rest))
		 (extract-vars (quasiquote-ellipses rest) form))
		((quasiquote-aux (,@form . rest))
		 (append form (quasiquote-aux rest)))
		((quasiquote-aux `form . depth)
		 (list 'quasiquote (quasiquote-aux form #f . depth)))
		((quasiquote-aux ,form #f . depth)
		 (list 'unquote (quasiquote-aux form . depth)))
		((quasiquote-aux ,@form #f . depth)
		 (list 'unquote-splicing (quasiquote-aux form . depth)))
		((quasiquote-aux (car . cdr) . depth)
		 (cons (quasiquote-aux car . depth) (quasiquote-aux cdr . depth)))
		((quasiquote-aux #(element :::) . depth)
		 (list->vector (quasiquote-aux (element :::) . depth)))
		((quasiquote-aux constant . depth)
		 'constant)))

(define-syntax quasiquote-ellipses-repeated
  (syntax-rules ()
    ((quasiquote-ellipses rest () form)
     (syntax-error "no substitutions to repeat here"))    
    ((quasiquote-ellipses-repeated rest ((var init) ...) form)
     (append (append-map (lambda (var ...)
			   `form)
			 init ...)
	     `rest))))

(define-syntax quasiquote-ellipses
  (syntax-rules ()
    ((quasiquote-ellipses rest () form)
     (syntax-error "no substitutions to repeat here"))    
    ((quasiquote-ellipses rest ((var init) ...) form)
     (append (map (lambda (var ...)
		    `form)
		  init ...)
	     `rest))))

(define-syntax extract-vars
  (syntax-rules ::: (quasiquote unquote unquote-splicing ...)
    ((extract-vars (k :::) ,form)
     (k ::: ((tmp form)) ,tmp))

    ((extract-vars (k :::) ,@form)
     (k ::: ((tmp form)) ,@tmp))

    ((extract-vars (k :::) `form . depth)
     (extract-vars (extract-vars "quasiquote" (k :::)) form #f . depth))
    ((extract-vars "quasiquote" (k :::) ((var init) :::) form)
     (k ::: ((var init) :::) `form))

    ((extract-vars (k :::) ,form #f . depth)
     (extract-vars (extract-vars "unquote" (k :::)) form . depth))
    ((extract-vars "unquote" (k :::) ((var init) :::) form)
     (k ::: ((var init) :::) ,form))

    ((extract-vars (k :::) ,@form #f . depth)
     (extract-vars (extract-vars "unquote-splicing" (k :::)) form . depth))
    ((extract-vars "unquote-splicing" (k :::) ((var init) :::) form)
     (k ::: ((var init) :::) ,@form))
    
    ((extract-vars (k :::) (car . cdr) . depth)
     (extract-vars (extract-vars "car" (k :::) cdr depth) car . depth))
    ((extract-vars "car" (k :::) cdr depth ((var init) :::) car)
     (extract-vars (extract-vars "cdr" (k :::) ((var init) :::) car) cdr . depth))
    ((extract-vars "cdr" (k :::) ((var1 init1) :::) car ((var2 init2) :::) cdr)
     (k ::: ((var1 init1) ::: (var2 init2) :::) (car . cdr)))

    ((extract-vars (k :::) #(element :::) . depth)
     (extract-vars (extract-vars "vector" (k :::)) (element :::) . depth))
    ((extract-vars "vector" (k :::) ((var init) :::) (element :::))
     (k ::: ((var init) :::) #(element :::)))

    ((extract-vars (k :::) constant . depth)
     (k ::: () constant))))

;;; Utility procedures

;; From SRFI 1.
(define (append-map proc . lists)
  (apply append (apply map proc lists)))
