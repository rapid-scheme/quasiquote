;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid quasiquote-test)
  (export run-tests)
  (import (except (scheme base) quasiquote)
	  (rapid test)
	  (rapid quasiquote))
  (begin
    (define (run-tests)
      (test-begin "Extended quasiquotation with ellipses")

      (test-group "Basic syntax"
	(define name 'a)
	(define name1 'x)
	(define name2 'y)
	(test-equal '(list 3 4) `(list ,(+ 1 2) 4))
	(test-equal '(list a (quote a)) `(list ,name ',name))
	(test-equal '(a 3 4 5 6 b) `(a ,(+ 1 2) ,@(map abs '(4 -5 6)) b))
	(test-equal #(10 5 4 16 9 8)
	  `#(10 5 ,(square 2) ,@(map square '(4 3)) 8))
	(test-equal '(a `(b ,(+ 1 2) ,(foo 4 d) e) f)
	  `(a `(b ,(+ 1 2) ,(foo ,(+ 1 3) d) e) f) )
	(test-equal '(a `(b ,x ,'y d) e)
	  `(a `(b ,,name1 ,',name2 d) e))
	(test-equal '(list 3 4) (quasiquote (list (unquote (+ 1 2)) 4)) )
	(test-equal `(list ,(+ 1 2) 4) (quasiquote (list (unquote (+ 1 2)) 4))))

      (test-group "Extended syntax"
	(define l '(1 2 3))
	(define ll '((a 1) (b 2) (c 3)))
	
	(test-equal "Escaping of ellipses"
	  '(a (... 3) b)
	  `(a (... (... ,(+ 1 2))) b))

	(test-equal "Ellipses are only special at the outermost nesting level"
	  '`(a (... ...) ,...)
	  ``(a (... ...) ,(... ...)))

	(test-equal "Repetition of simple subtemplate"
	  '(1 2 3)
	  `(,l ...))

	(test-equal "Repetition of structured subtemplate"
	  '((foo 1) (foo 2) (foo 3))
	  `((foo ,l) ...))

	(test-equal "Repetition of subtemplate with repetition"
	  `((foo a 1) (foo b 2) (foo c 3))
	  `((foo ,ll ...) ...))

	(test-equal "Repeated repetition"
	  '((foo a) (foo 1) (foo b) (foo 2) (foo c) (foo 3))
	  `((foo ,ll) ... ...))

	(test-equal "Repeated unquote-splicing"
	  '(a 1 b 2 c 3)
	  `(,@ll ...)))
    
      (test-end))))
